/* eslint-disable no-unused-vars */
/*
 * This file is picked up by the build system only
 * when building for PRODUCTION
 */

import { register } from 'register-service-worker';

register(process.env.SERVICE_WORKER_FILE, {
  ready() {
  },
  registered(registration) { // registration -> a ServiceWorkerRegistration instance
  },
  cached(registration) { // registration -> a ServiceWorkerRegistration instance
  },
  updatefound(registration) { // registration -> a ServiceWorkerRegistration instance
  },
  updated(registration) { // registration -> a ServiceWorkerRegistration instance
  },
  offline() {
  },
  error(err) {
  },
});

// ServiceWorkerRegistration: https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration
