import Vue from 'vue';
import VueRouter from 'vue-router';

import routes from './routes';
import app from '../firebase';

import store from '../store/index';

Vue.use(VueRouter);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE,
  });

  /**
   * Adds this.$currentUser
   * $currentUser.loading: Promise resolved when below data fetched...
   * $currentUser.roleData: Either carers/{carer}/... or services/{service}/...
   * $currentUser.data: Data stored in users/{uid}
   */
  // TODO This is a hack! Move it to a more logical location!
  Router.beforeEach(async (to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if (!app.auth().currentUser) {
        app.auth().onAuthStateChanged(async (user) => {
          if (!user) {
            next({
              path: '/login',
            });
          } else {
            const userRef = app.firestore().collection('users').doc(user.uid);
            await store().dispatch('user/loadUser', userRef);
            next();
          }
        });
      } else {
        next();
      }
    } else {
      next();
    }
  });

  return Router;
}
