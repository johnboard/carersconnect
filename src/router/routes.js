const routes = [
  /**
   * Root Path
   * Landing page after login will render home page in mobile layout
   * With the requiresAuth meta, it won't render if the user is not logged in
   * ^ See routes/index.js:26
   */
  {
    path: '/',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Home.vue') },
    ],
    meta: {
      requiresAuth: true,
    },
  },
  /**
   * Home page for the services
   * TODO - This should be merged with /:
   * TODO - if user, show Home.vue, if service show ServiceHome.vue
   */
  {
    path: '/servicehome',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/service/ServiceHome.vue') },
    ],
    meta: {
      requiresAuth: true,
    },
  },
  /**
   * Shows carer & service login
   */
  {
    path: '/login',
    component: () => import('pages/Login.vue'),
  },
  /**
   * Lists all the services
   */
  {
    path: '/services',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/ServiceList.vue') },
    ],
  },
  /**
   * Community Hub
   */
  {
    path: '/community',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Community.vue') },
    ],
  },
  /**
   * Helplines
   */
  {
    path: '/helplines',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Helplines.vue') },
    ],
  },
    /**
   * Authorisation
   */
  {
    path: '/authorise',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Authorise.vue') },
    ],
  },
      /**
   * Authorisation
   */
  {
    path: '/authoriseSign',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/AuthoriseSign.vue') },
    ],
  },
  {
    path: '/authoriseSignApple',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/AuthoriseSignApple.vue') },
    ],
  },
  {
    path: '/authoriseSignExchange',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/AuthoriseSignExchange.vue') },
    ],
  },
  {
    path: '/authoriseSignOffice',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/AuthoriseSignOffice.vue') },
    ],
  },
  /**
   * Questions About Dementia
   */
  {
    path: '/questions',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Questions.vue') },
    ],
  },
  /**
   * Questions About Dementia
   */
  {
    path: '/questionsbranch1',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/QuestionsBranch1.vue') },
    ],
  },
  /**
   * Questions About Dementia
   */
  {
    path: '/article4',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Article4.vue') },
    ],
  },
  /**
   * Questions About Dementia
   */
  {
    path: '/article1',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Article1.vue') },
    ],
  },
  /**
   * Questions About Dementia
   */
  {
    path: '/article2',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Article2.vue') },
    ],
  },
  /**
   * Questions About Dementia
   */
  {
    path: '/article3',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Article3.vue') },
    ],
  },
  /**
   * List of patients for service
   */
  {
    path: '/patients/list',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/service/PatientList.vue') },
    ],
    meta: {
      requiresAuth: true,
    },
  },
  /**
   * Patient Profile
   */
  {
    path: '/patient/:patientID',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/patient/PatientProfile.vue') },
    ],
    meta: {
      requiresAuth: false,
    },
  },
  /**
   * Profile for User & Service
   */
  {
    path: '/profile',
    component: () => import('layouts/CarerMobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/carer/CarerProfile.vue') },
    ],
    meta: {
      requiresAuth: true,
    },
  },
  /**
   * Profile for service - should be merged with profile
   * TODO
   */
  {
    path: '/serviceprofile',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/ServiceProfile.vue') },
    ],
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/create/user',
    component: () => import('pages/account/UserCreation.vue'),
  },
  {
    path: '/create/service',
    component: () => import('pages/account/ServiceCreation.vue'),
  },
  /**
   * Registration Routes
   *  -> Carer
   *  -> Service
   */
  {
    path: '/register/carer',
    name: 'registerCarer',
    component: () => import('pages/account/UserRegistration.vue'),
  },
  {
    path: '/changepass',
    name: 'ChangePassword',
    component: () => import('pages/account/ChangePassword.vue'),
  },
  {
    path: '/register/service',
    name: 'registerService',
    component: () => import('pages/account/ServiceRegistration.vue'),
  },
  {
    path: '/test',
    name: 'peepo',
    component: () => import('components/peepoTest.vue'),
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue'),
  });
}

export default routes;
