export default {
  services: {
    // Key to indentify the service
    '.key': {
      type: 'uniqueID',
      required: true,
    },

    // Name
    name: {
      type: 'text',
      required: true,
    },

    // Type of practice
    type: {
      type: 'text',
      required: true,
    },

    // Contact details
    contact: {
      type: 'object',
      phone: {
        type: 'string',
        required: true,
      },
      email: {
        type: 'string',
        required: true,
      },
      address: {
        type: 'string',
        required: true,
      },
    },

    // The actual rating of the practice
    rating: {
      type: 'float',
      required: false,
      // Averages rating to be rating - calculator method
      calculate: ratings => 0
        .chain(ratings)
        .map(rating => rating.stars)
        .sum()
        .value() / ratings.length,
    },

    // Array that holds all the ratings
    ratings: [
      {
        reviewerUID: {
          type: 'string',
          required: 'true',
        },
        stars: {
          type: 'number',
          required: true,
        },
      },
      // ...
    ],
  },
  hotlines: {
    // Key to indentify the hotline
    '.key': {
      type: 'uniqueID',
      required: true,
    },

    // Name
    name: {
      type: 'string',
      required: true,
    },

    // Phone number
      phone: {
        type: 'string',
        required: true,
      },
    },

  patients: [{
    '.key': {
      type: 'uniqueID',
      required: true,
    },
    name: {
      type: 'text',
      required: true,
    },
    dateOfBirth: {
      type: 'date',
      required: true,
    },
    age: {
      type: 'number',
      calculate(birthday) {
        const ageDifMs = Date.now() - birthday.getTime();
        const ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
      },
    },
    carers: [
      // carer['.key']
    ],
    prescriptions: [
      {
        type: 'object',
        name: {
          type: 'text',
          required: true,
        },
        dosage: {
          type: 'number',
          required: true,
          suffix: 'mg',
        },
        instructions: {
          type: 'text',
        },
      },
    ],
  }],
  carers: [{
    '.key': {
      type: 'uniqueID',
      required: true,
    },
    patients: [
      // patient['.key']
    ],
  }],
  users: [{
    '.key': {
      type: 'uniqueID',
      required: true,
    },
    name: {
      type: 'text',
      required: true,
    },
    authorised: {
      type: 'text',
    },
    role: {
      type: 'text',
      options: [
        'carer',
        'patient',
        'staff',
      ],
    },
  }],
};
