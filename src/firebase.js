/* eslint-disable import/no-duplicates */
import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

import Vue from 'vue';
import VueFirestore from 'vue-firestore';

import AppConfig from './config';
import router from './router/index';

import * as VueGoogleMaps from 'vue2-google-maps';

/**
 * Configuration settings for firebase connectivity
 * @type {{apiKey: string, authDomain: string, databaseURL: string,
 *    projectId: string, storageBucket: string, messagingSenderId: string}}
 */

/**
 * Initialises VueFire plugin
 */
Vue.use(VueFirestore);

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyB26nhWoU4xYP8JuKngOWLIdJYQVj-FbhA',
    libraries: 'places',
  },
});
/**
 * Exports 'app' - an initialised firebase instanace used by the rest of the program
 * @type {firebase.app.App}
 */
const app = firebase.initializeApp(AppConfig.FIREBASE_CONFIG);
export default app;
Vue.prototype.$firebase = app;
Vue.prototype.$firestore = app.firestore;

/**
 * Deals with user logging out
 */
firebase.auth().onAuthStateChanged((user) => {
  if (!user) {
    router().replace('login');
  }
});

/**
 * Required code to stop error in update of firestore
 */
app.firestore().settings({ timestampsInSnapshots: true });
