export default Object.freeze({
  DEBUG_MODE: false,
  FIREBASE_CONFIG: {
    apiKey: 'AIzaSyB26nhWoU4xYP8JuKngOWLIdJYQVj-FbhA',
    authDomain: 'carersconnectdjlm.firebaseapp.com',
    databaseURL: 'https://carersconnectdjlm.firebaseio.com',
    projectId: 'carersconnectdjlm',
    storageBucket: 'carersconnectdjlm.appspot.com',
    messagingSenderId: '708617957961',
  },
});
