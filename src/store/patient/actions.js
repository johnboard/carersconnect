import { firestoreAction } from 'vuexfire';
import app from '../../firebase';

import firebase from 'firebase/app';
import 'firebase/firestore';

const fs = app.firestore();

export async function waitForLoaded({ state }, instance) {
  return new Promise((resolve) => {
    if (state.$loaded) {
      resolve(state);
    } else {
      const unwatch = instance.$watch('$store.state.patient.$loaded', () => {
        unwatch();
        resolve(state);
      });
    }
  });
}

/**
 * Loads a patient, and their associated properties
 */
export const loadPatient = firestoreAction(async ({ dispatch, commit, bindFirestoreRef }, ref) => {
  const data = await bindFirestoreRef('data', ref);
  commit('setLoaded', true);

  await dispatch('loadServices');
  await dispatch('loadHotlines');

  return data;
});

export async function loadServices({ dispatch, commit }) {
  const services = await dispatch('getServices');
  commit('setServices', services);
  return services;
}
export async function loadHotlines({ dispatch, commit }) {
  const hotlines = await dispatch('getHotlines');
  commit('setHotlines', hotlines);
  return hotlines;
}

/**
 * Returns patients associated with currently logged in user
 * @param state
 * @returns {Promise<void>}
 */
export async function getServices({ state }) {
  // This is what gets returned...
  const serviceList = {};

  /**
   * await runs this function sync'ly so that the return statement doesn't return
   * patientList = {}, but rather waits for patientList to be populated.
   */
  await fs.collection('services')
    .where('patients', 'array-contains', state.data.id)
    .get()
    .then((snapshot) => {
      // For each of the documents matching the query...
      snapshot.forEach((doc) => {
        // Populate patientList
        serviceList[doc.id] = doc.data();
      });
    });
  return serviceList;
}

/**
 * Returns hotlines associated with currently logged in user
 * @param state
 * @returns {Promise<void>}
 */
export async function getHotlines({ state }) {
  // This is what gets returned...
  const hotlineList = {};

  /**
   * await runs this function sync'ly so that the return statement doesn't return
   * patientList = {}, but rather waits for patientList to be populated.
   */
  await fs.collection('hotlines')
    .where('hotlines', 'array-contains', state.data.id)
    .get()
    .then((snapshot) => {
      // For each of the documents matching the query...
      snapshot.forEach((doc) => {
        // Populate patientList
        hotlineList[doc.id] = doc.data();
      });
    });
  return hotlineList;
}

export async function addService({ state, dispatch }, serviceID) {
  await fs.collection('services')
    .doc(serviceID)
    .update({
      patients: firebase.firestore.FieldValue.arrayUnion(state.data.id),
    });
  fs.collection('patients')
    .doc(state.data.id)
    .update({
      services: firebase.firestore.FieldValue.arrayUnion(serviceID),
    });
  dispatch('loadServices');
}

export async function addHotline({ state, dispatch }, hotlineID) {
  await fs.collection('hotlines')
    .doc(hotlineID)
    .update({
      patients: firebase.firestore.FieldValue.arrayUnion(state.data.id),
    });
  fs.collection('patients')
    .doc(state.data.id)
    .update({
      services: firebase.firestore.FieldValue.arrayUnion(hotlineID),
    });
  dispatch('loadHotlines');
}

export async function removeService({ state, dispatch }, serviceID) {
  await fs.collection('services')
    .doc(serviceID)
    .update({
      patients: firebase.firestore.FieldValue.arrayRemove(state.data.id),
    });
  fs.collection('patients')
    .doc(state.data.id)
    .update({
      services: firebase.firestore.FieldValue.arrayRemove(serviceID),
    });
  await dispatch('loadServices');
}
