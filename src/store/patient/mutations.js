export function setLoaded(state, loaded) {
  state.$loaded = loaded;
}

export function setServices(state, services) {
  state.services = services;
}

export function setHotlines(state, hotlines) {
  state.hotlines = hotlines;
}
