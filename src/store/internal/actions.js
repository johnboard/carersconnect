// import { firebaseAction } from 'vuexfire';
import app from '../../firebase';
import { firebaseAction } from 'vuexfire';

const fs = app.firestore();

/**
 * Loads a patient, and their associated properties
 */
export const loadService = firebaseAction(async ({ bindFirebaseRef }, ref) => {
  const data = await bindFirebaseRef('service', ref);
  return data;
});

export const loadHotline = firebaseAction(async ({ bindFirebaseRef }, ref) => {
  const data = await bindFirebaseRef('hotline', ref);
  return data;
});

/**
 * Loads list of patients of the current service into the store under $store.state.internal.patients
 * @param dispatch
 * @param commit
 * @returns {Promise<*>}
 */
export async function loadServicePatients({ dispatch, commit }) {
  const patients = await dispatch('getServicePatients');
  commit('setServicePatients', patients);
  return patients;
}

/**
 * Returns list of patients of the current service
 * @param state
 * @returns {Promise<void>}
 */
export async function getServicePatients({ state }) {
  const patientsQuery = await fs.collection('patients').where('services', 'array-contains', state.service.id).get();
  const list = {};
  patientsQuery.docs.forEach((patient) => {
    list[patient.id] = patient.data();
  });
  return list;
}
