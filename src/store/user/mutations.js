export function setLoaded(state, loaded) {
  state.$loaded = loaded;
}

export function setPatients(state, patients) {
  state.patients = patients;
}
