import { firestoreAction } from 'vuexfire';
import firebase from '../../firebase';

const fs = firebase.firestore();

export async function waitForLoaded({ state }, instance) {
  return new Promise((resolve) => {
    if (state.$loaded) {
      resolve(state);
    } else {
      const unwatch = instance.$watch('$store.state.user.$loaded', () => {
        unwatch();
        resolve(state);
      });
    }
  });
}

/**
 * Loads a user, and their associated properties
 */
export const loadUser = firestoreAction(async ({ dispatch, commit, bindFirestoreRef }, ref) => {
  console.log(ref);
  const data = await bindFirestoreRef('data', ref);
  // Load patients while you're at it...
  const patients = await dispatch('getPatients');
  commit('setPatients', patients);
  commit('setLoaded', true);
  return data;
});


/**
 * Set the name of a specified user
 * @param state
 * @param name
 */
export function setUserName({ state }, name) {
  fs.collection('users').doc(state.user['.key'])
    .update({
      name,
    });
}

/**
 * Updates a user's profile to the param profile
 * @param state
 * @param profile
 */
export function setUserProfile({ state }, profile) {
  fs.collection('users').doc(state.data.id)
    .set(profile);
}

/**
 * Returns patients associated with currently logged in user
 * @param state
 * @returns {Promise<void>}
 */
export async function getPatients({ state }) {
  // This is what gets returned...
  const patientList = {};

  /**
   * await runs this function sync'ly so that the return statement doesn't return
   * patientList = {}, but rather waits for patientList to be populated.
   */
  await fs.collection('patients')
    .where('carers', 'array-contains', state.data.id)
    .get()
    .then((snapshot) => {
      // For each of the documents matching the query...
      snapshot.forEach((doc) => {
        // Populate patientList
        patientList[doc.id] = doc.data();
      });
    });
  return patientList;
}
