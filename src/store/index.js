import Vue from 'vue';
import Vuex from 'vuex';
import { vuexfireMutations } from 'vuexfire';

/**
 * Module Imports
 */
import user from './user';
import patient from './patient';
import internal from './internal';

Vue.use(Vuex);


export default function () {
  const Store = new Vuex.Store({
    modules: {
      patient,
      user,
      internal,
    },
    mutations: vuexfireMutations,
  });
  return Store;
}
