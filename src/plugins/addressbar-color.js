import { AddressbarColor } from 'quasar';

export default () => {
  AddressbarColor.set('rgb(0, 150, 136)');
};
