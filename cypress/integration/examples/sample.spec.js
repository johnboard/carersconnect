describe('Test login', () => {
  it('Logs in', () => {
    Cypress.on('uncaught:exception', (err, runnable) => false);
    cy.visit('http://localhost:8080/');

    cy.get('.form-email > div > div > input').type('example@example.com');
    cy.get('.form-password > div > div > input').type('example');
    cy.get('.form-submit').click();

    cy.wait(3000);
  });
});
