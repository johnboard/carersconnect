const functions = require('firebase-functions');
const admin = require('firebase-admin');
const sgMail = require('@sendgrid/mail');

admin.initializeApp();

/**
 * There's a many to many relationship between patients and carers.
 * This function ensures that when carer is added to a patient,
 * that the patient is added to the carer.
 * @type {CloudFunction<DocumentSnapshot>}
 */
/*
exports.addPatientCarer = functions.firestore
  .document('users/{userID}/carers/{carerID}')
  .onCreate((snap, context) => {
    return admin.firestore()
      .collection('carers')
      .doc(snap.data().id)
      .collection('patients')
      .doc(context.params.userID)
      .set({ linked: true });
  });
*/

/**
 * Returns a configured SendGrid client.
 *
 * @param {string} key Your SendGrid API key.
 * @returns {object} SendGrid client.
 */
exports.sendPatientInfoEmail = functions.https.onRequest((req, res) => {
  res.set('Access-Control-Allow-Origin', '*');
  const API_KEY = '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';

  const email = req.query.email;
  const name = req.query.name;
  const link = req.query.link;
  const password = req.query.password;

  sgMail.setApiKey(API_KEY);
  const msg = {
    "template_id":"d-075a8c6aff324362b0e14742eb9d01d3",
    from: {
      name: "CarersConnect",
      email: 'donotreply@eante.com.au'
    },
    "personalizations":[
      {
        to: [ { email }],
        "dynamic_template_data": {
          name, link, password,
        }
      },
    ]
  };
  sgMail.send(msg).then(() => {
    res.status(200).send('OK');
  });
});
