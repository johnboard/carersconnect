module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  env: {
    browser: true
  },
  // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
  // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
  extends: ['plugin:vue/essential', 'airbnb-base'],
  // required to lint *.vue files
  plugins: [
    'vue'
  ],
  globals: {
    'ga': true, // Google Analytics
    'cordova': true,
    '__statics': true
  },
  // add your custom rules here
  'rules': {
    'no-param-reassign': 0,
    'linebreak-style': 0,
    'import/first': 0,
    'import/named': 2,
    'import/namespace': 2,
    'import/default': 2,
    'import/export': 2,
    'import/extensions': 0,
    'import/no-unresolved': 0,
    'import/no-extraneous-dependencies': 0,
    'max-len': 0,
    'prefer-template': 0,
    'arrow-parens': 0,
    'no-return-assign': 0,
    'space-before-function-paren': 0,
    'no-trailing-spaces': 0,
    'spaced-comment': 0,
    'indent': 0,
    'quotes': 0,
    'no-bitwise': 0,
    'no-undef': 0,
    'no-void': 0,
    'one-var': 0,
    'no-plusplus': 0,
    'function-paren-newline': 0,
    'no-unused-vars': 0,
    'global-require':0,
    'brace-style': 0,
    'no-useless-return': 0,
    'no-else-return': 0,
    'vue/no-side-effects-in-computed-properties': 0,


    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  }
}
